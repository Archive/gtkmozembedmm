/* 
 * main.cc - HTMLView main window and startup module
 *
 * Copyright (C) 2004 Max Vasin
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */
//------------------------------------------------------------------------------
#include <iomanip>
#include <iostream>
#include <sstream>
#include <gtkmm.h>
#include <libgnomevfsmm.h>
//------------------------------------------------------------------------------
#include "TabManager.h"
//------------------------------------------------------------------------------
namespace HTMLView
{
  class MainWindow : public Gtk::Window
  {
    Glib::RefPtr<Gtk::Action> bwd_ac;
    Glib::RefPtr<Gtk::Action> fwd_ac;

    Glib::RefPtr<Gtk::ActionGroup> actions;
    Glib::RefPtr<Gtk::UIManager> uimanager;
    Gtk::Menu* popup;

    Gtk::VBox vbox;
    HTMLView::TabManager tab_manager;
    Gtk::Statusbar status_bar;
    Gtk::FileChooserDialog file_load_dialog;

    void read_settings()
    {
      class SettingsParser : public Glib::Markup::Parser
      {
	TabManager& tab_manager;
      public:
	explicit SettingsParser(TabManager& tab_manager)
	  : tab_manager(tab_manager)
	{ }
	
	virtual void on_start_element(Glib::Markup::ParseContext& ctx, const Glib::ustring& name,
				      const Glib::Markup::Parser::AttributeMap& attrs)
	{
	  Glib::Markup::Parser::AttributeMap::const_iterator idir = attrs.find("dir");
	  Glib::Markup::Parser::AttributeMap::const_iterator iname = attrs.find("name");

	  if (name == "profile" && idir != attrs.end() && iname != attrs.end())
	    Mozilla::WebControl::set_profile_path(idir->second, iname->second);
	  else if (name == "comp" && idir != attrs.end())
	    Mozilla::WebControl::set_comp_path(idir->second);
	  else if (name == "extmime" && iname != attrs.end())
	    tab_manager.add_external_mime_type(iname->second);
	  else if (name == "extscheme" && iname != attrs.end())
	    tab_manager.add_external_scheme(iname->second);
	  else if (name == "exthost" && iname != attrs.end())
	    tab_manager.add_external_host(iname->second);
	  else if (name == "gvfs_load_scheme" && iname != attrs.end())
	    tab_manager.add_gvfs_load_scheme(iname->second);
	  else
	    std::cerr << "error: unknown or incorrect element: " << name << "at line " << ctx.get_line_number() 
		      << " - ignoring" << std::endl;
	}
      };

      try
	{
	  SettingsParser parser(tab_manager);
	  Glib::Markup::ParseContext ctx(parser);
	  Glib::RefPtr<Glib::IOChannel> ic 
	    = Glib::IOChannel::create_from_file(Glib::build_filename(Glib::get_home_dir(), ".htmlviewrc"), "r");
	  Glib::ustring config;
	  ic->read_to_end(config);
	  ctx.parse(config);
	}
      catch (Glib::FileError& err)
	{
	  std::cerr << "htmlview: unable to read config file: " << err.what() << std::endl;
	}
    }
  public:
    explicit MainWindow()
      : actions(Gtk::ActionGroup::create()),
	uimanager(Gtk::UIManager::create()),
	file_load_dialog("Open File")
    { 
      read_settings();

      set_title("HTMLView " VERSION);
      maximize();

      file_load_dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
      file_load_dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

      // `File' Menu
      actions->add(Gtk::Action::create("FileMenu", "File"));
      actions->add(Gtk::Action::create("OpenFile", Gtk::Stock::OPEN),
		   sigc::bind<bool>(sigc::mem_fun(*this, &MainWindow::load_file), false));
      actions->add(Gtk::Action::create("OpenFileInNewTab", "Open File in New Tab"),
		   sigc::bind<bool>(sigc::mem_fun(*this, &MainWindow::load_file), true));
      actions->add(Gtk::Action::create("OpenURL", "Open URL"),
      		   sigc::mem_fun(*this, &MainWindow::load_url));
      actions->add(Gtk::Action::create("ShowNetLog", "Show NetLog"),
		   sigc::mem_fun(tab_manager, &TabManager::show_log));
      actions->add(Gtk::Action::create("Quit", Gtk::Stock::QUIT),
		   sigc::mem_fun(*this, &Gtk::Widget::hide));
      // `Go' Menu
      actions->add(Gtk::Action::create("GoMenu", "Go"));
      actions->add(Gtk::Action::create("GoBack", Gtk::Stock::GO_BACK),
		   sigc::mem_fun(tab_manager, &TabManager::go_back));
      actions->add(Gtk::Action::create("GoForward", Gtk::Stock::GO_FORWARD),
		   sigc::mem_fun(tab_manager, &TabManager::go_forward));
      actions->add(Gtk::Action::create("StopLoad", Gtk::Stock::STOP),
		   sigc::mem_fun(tab_manager, &TabManager::stop_load));
      actions->add(Gtk::Action::create("ReloadMenu", "Reload"));
      actions->add(Gtk::Action::create("ReloadNormal", Gtk::Stock::REFRESH, "Reload Normal"),
		   sigc::bind<Mozilla::ReloadFlags>(sigc::mem_fun(tab_manager, &TabManager::reload), 
						    Mozilla::WEB_CONTROL_FLAG_RELOAD_NORMAL));
      actions->add(Gtk::Action::create("ReloadBypassCache", "Reload Bypass Cache"),
		   sigc::bind<Mozilla::ReloadFlags>(sigc::mem_fun(tab_manager, &TabManager::reload), 
						    Mozilla::WEB_CONTROL_FLAG_RELOAD_BYPASS_CACHE));
      actions->add(Gtk::Action::create("ReloadBypassProxy", "Reload Bypass Proxy"),
		   sigc::bind<Mozilla::ReloadFlags>(sigc::mem_fun(tab_manager, &TabManager::reload), 
						    Mozilla::WEB_CONTROL_FLAG_RELOAD_BYPASS_PROXY));
      actions->add(Gtk::Action::create("ReloadBypassProxyAndCache", "Reload Bypass Proxy and Cache"),
		   sigc::bind<Mozilla::ReloadFlags>(sigc::mem_fun(tab_manager, &TabManager::reload), 
						    Mozilla::WEB_CONTROL_FLAG_RELOAD_BYPASS_PROXY_AND_CACHE));
      // `Help' Menu
      actions->add(Gtk::Action::create("HelpMenu", "Help"));
      actions->add(Gtk::Action::create("About", "About"),
		   sigc::mem_fun(tab_manager, &TabManager::show_about));

      const Glib::ustring ui_info = 
	"<ui>"
	"  <menubar name='MenuBar'>"
	"    <menu action='FileMenu'>"
	"      <menuitem action='OpenFile'/>"
	"      <menuitem action='OpenFileInNewTab'/>"
	"      <menuitem action='OpenURL'/>"
	"      <menuitem action='ShowNetLog'/>"
	"      <separator/>"
	"      <menuitem action='Quit'/>"
	"    </menu>"
	"    <menu action='GoMenu'>"
	"      <menuitem action='GoBack'/>"
	"      <menuitem action='GoForward'/>"
	"      <separator/>"
	"      <menuitem action='StopLoad'/>"
	"      <menu action='ReloadMenu'>"
	"      <menuitem action='ReloadNormal'/>"
	"      <menuitem action='ReloadBypassCache'/>"
	"      <menuitem action='ReloadBypassProxy'/>"
	"      <menuitem action='ReloadBypassProxyAndCache'/>"
	"      </menu>"
	"    </menu>"
	"    <menu action='HelpMenu'>"
	"      <menuitem action='About'/>"
	"    </menu>"
	"  </menubar>"
	"  <toolbar name='ToolBar'>"
	"    <toolitem action='OpenFile'/>"
	"    <toolitem action='GoBack'/>"
	"    <toolitem action='GoForward'/>"
	"    <toolitem action='ReloadNormal'/>"
	"    <toolitem action='StopLoad'/>"
	"  </toolbar>"
	"  <popup name='Popup'>"
	"      <menuitem action='GoBack'/>"
	"      <menuitem action='GoForward'/>"
	"      <separator/>"
	"      <menuitem action='OpenFile'/>"
	"      <menuitem action='OpenFileInNewTab'/>"
	"      <menuitem action='OpenURL'/>"
	"  </popup>"
	"</ui>";
      uimanager->insert_action_group(actions);
      uimanager->add_ui_from_string(ui_info);
      add_accel_group(uimanager->get_accel_group());
      popup = dynamic_cast<Gtk::Menu*>(uimanager->get_widget("/Popup"));

      bwd_ac = actions->get_action("GoBack");
      fwd_ac = actions->get_action("GoForward");
      fwd_ac->set_sensitive(false);
      bwd_ac->set_sensitive(false);

      tab_manager.signal_location().connect(sigc::mem_fun(*this, &MainWindow::location_changed));
      tab_manager.signal_title().connect(sigc::mem_fun(*this, &MainWindow::change_title));
      tab_manager.signal_status_message().connect(sigc::mem_fun(*this, &MainWindow::status_message));
      tab_manager.signal_popup().connect(sigc::mem_fun(*this, &MainWindow::show_popup_menu));

      add(vbox);
      vbox.pack_start(*uimanager->get_widget("/MenuBar"), Gtk::PACK_SHRINK);
      vbox.pack_start(*uimanager->get_widget("/ToolBar"), Gtk::PACK_SHRINK);
      vbox.pack_start(tab_manager);
      vbox.pack_end(status_bar, Gtk::PACK_SHRINK);
      
      show_all();
      tab_manager.load_in_current_tab("about:blank");
    }

  private:
    class OpenURLDialog : public Gtk::Dialog
    {
      Gtk::Entry url_entry;
    public:
      OpenURLDialog()
	: Dialog("Open URL")
      {
	Gtk::VBox& vbox = *get_vbox();
	vbox.pack_start(*manage(new Gtk::Label("Enter URL to load")), Gtk::PACK_SHRINK);
	vbox.pack_start(url_entry, Gtk::PACK_SHRINK);
	show_all();

	add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
	add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	hide();
      }

      const Glib::ustring get_url() const
      {
	return url_entry.get_text();
      }

      void set_url(const Glib::ustring& url)
      {
	url_entry.set_text(url);
      }
    };

    OpenURLDialog open_url_dialog;

    void change_title(const Glib::ustring& title)
    {
      set_title("HTMLView - " + title);
    }

    void load_url()
    {
      int resp = open_url_dialog.run();
      open_url_dialog.hide();

      if (resp == Gtk::RESPONSE_OK) 
	tab_manager.load_in_current_tab(open_url_dialog.get_url());
    }

    void load_file(bool in_new_tab)
    { 
      file_load_dialog.set_select_multiple(in_new_tab);
      const int resp = file_load_dialog.run();
      file_load_dialog.hide();
      if (resp == Gtk::RESPONSE_OK)
	if (in_new_tab)
	  {
	    Glib::SListHandle<Glib::ustring> uris = file_load_dialog.get_uris();
	    for (Glib::SListHandle<Glib::ustring>::const_iterator i = uris.begin();
		 i != uris.end(); ++i)
	      tab_manager.new_tab(*i);
	  }
	else
	  tab_manager.load_in_current_tab(file_load_dialog.get_uri());
    }

    void location_changed(const Glib::ustring& location)
    {
      open_url_dialog.set_url(location);
      fwd_ac->set_sensitive(tab_manager.can_go_forward());
      bwd_ac->set_sensitive(tab_manager.can_go_back());
    }

    void status_message(const Glib::ustring& msg)
    {
      status_bar.pop();
      status_bar.push(msg);
    }

    void show_popup_menu()
    {
      popup->popup(3, 0);
    }
  };
}
//------------------------------------------------------------------------------
int main(int argc, char** argv)
{
  Gtk::Main kit(argc, argv);
  Gnome::Vfs::init();
  HTMLView::MainWindow mw;
  Gtk::Main::run(mw);
  return 0;
}
//------------------------------------------------------------------------------
