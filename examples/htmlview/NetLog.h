/* 
 * NetLog.h - HTMLView network events log
 *
 * Copyright (C) 2004 Max Vasin
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */
//------------------------------------------------------------------------------
#ifndef HTMLVIEW_NET_LOG_H
#define HTMLVIEW_NET_LOG_H
//------------------------------------------------------------------------------
#include <gtkmm.h>
#include <gtkmozembedmm.h>
//------------------------------------------------------------------------------
namespace HTMLView
{
  class NetLog : public Gtk::Window
  {
    Gtk::ScrolledWindow scroll_win;
    Gtk::TextView log_view;
    Glib::RefPtr<Gtk::TextBuffer> log_buffer;
    void append_line(const Glib::ustring& text);
  public:
    NetLog();
    void log_state(const Glib::ustring& aUri, Mozilla::ProgressFlags state, Mozilla::StatusFlags status);
  };
}
//------------------------------------------------------------------------------
#endif // HTMLVIEW_NET_LOG_H
//------------------------------------------------------------------------------
/*
 * Local Variables:
 * mode: c++
 * End:
 */
