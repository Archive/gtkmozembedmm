/* 
 * TabManager.h - HTMLView tab manager 
 *
 * Copyright (C) 2004 Max Vasin
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */
//------------------------------------------------------------------------------
#ifndef HTMLVIEW_TAB_MANAGER_H
#define HTMLVIEW_TAB_MANAGER_H
//------------------------------------------------------------------------------
#include <iostream>
#include <set>
#include <vector>
#include <gtkmm.h>
#include <gtkmozembedmm.h>
//------------------------------------------------------------------------------
#include "NetLog.h"
//------------------------------------------------------------------------------
namespace HTMLView
{
  class TabManager : public Gtk::Notebook
  {
    NetLog net_log;
    std::set<Glib::ustring> external_mimes;     // MIME types to use external handlers for
    std::set<Glib::ustring> external_schemes;   // schemes to use external handlers for
    std::set<Glib::ustring> external_hosts;     // hosts to use external handlers for
    std::set<Glib::ustring> gvfs_load_schemes;  // schemes to use GnomeVFS to open uris

    Mozilla::WebControl& get_web_control();
    bool open_uri_with_gvfs(Glib::RefPtr<Gnome::Vfs::Uri> uri);

    /*
     * Signal handlers
     */
    void title(int idx);
    void location(int idx);
    void link_message(int idx);
    bool open_uri(const Glib::ustring& uri);
    Mozilla::WebControl* new_window(Mozilla::ChromeFlags);
    void switch_page(GtkNotebookPage*, guint);
    void net_start(int idx);
    void net_stop(int idx);
    gint dom_mouse_click(nsCOMPtr<nsIDOMMouseEvent> dom_mouse_event);
    void progress(int cur, int max, int idx);

    /*
     * Provided signals
     */
    sigc::signal<void, Glib::ustring> m_signal_title;
    sigc::signal<void, Glib::ustring> m_signal_location;
    sigc::signal<void, Glib::ustring> m_signal_status_message;
    sigc::signal<void>                m_signal_popup;
  public:
    TabManager();

    Mozilla::WebControl* new_tab(const Glib::ustring& url = "");
    void load_in_current_tab(const Glib::ustring& url);
    void show_about();

    bool can_go_forward() { return get_web_control().can_go_forward(); }
    bool can_go_back   () { return get_web_control().can_go_back();    }
    void go_forward    () { get_web_control().go_forward();            }
    void go_back       () { get_web_control().go_back();               }
    void show_log      () { net_log.show();                            }
    void stop_load     () { get_web_control().stop_load();             }
    void reload(Mozilla::ReloadFlags rf) { get_web_control().reload(rf); }

    sigc::signal<void, Glib::ustring> signal_title         () { return m_signal_title;          }
    sigc::signal<void, Glib::ustring> signal_location      () { return m_signal_location;       }
    sigc::signal<void, Glib::ustring> signal_status_message() { return m_signal_status_message; }
    sigc::signal<void>                signal_popup         () { return m_signal_popup;          }

    void add_external_mime_type(const Glib::ustring& mime_type) { external_mimes.insert(mime_type); }
    void add_external_scheme   (const Glib::ustring& scheme)    { external_schemes.insert(scheme);  }
    void add_external_host     (const Glib::ustring& host)      { external_hosts.insert(host);      }
    void add_gvfs_load_scheme  (const Glib::ustring& scheme)    { gvfs_load_schemes.insert(scheme); }
  };
}
//------------------------------------------------------------------------------
#endif // HTMLVIEW_TAB_MANAGER_H
//------------------------------------------------------------------------------
/*
 * Local Variables:
 * mode: c++
 * End:
 */
