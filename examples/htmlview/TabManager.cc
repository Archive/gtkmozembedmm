/* 
 * TabManager.cc - HTMLView tab manager 
 *
 * Copyright (C) 2004 Max Vasin
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */
//------------------------------------------------------------------------------
#include <iostream>
#include <libgnomevfsmm.h>
//------------------------------------------------------------------------------
#include "TabManager.h"
//------------------------------------------------------------------------------
namespace
{
  class TabLabel : public Gtk::Table
  {
    Gtk::Label label;
    Gtk::ProgressBar pb;
  public:
    explicit TabLabel(const Glib::ustring& text)
      : label(text)
    { 
      attach(label, 0, 1, 0, 1);
      attach(pb, 0, 1, 0, 1);
      label.show();
      pb.show();
    }

    void show_progress()
    {
      pb.show();
    }

    void hide_progress()
    {
      pb.hide();
    }

    void update(int cur, int max)
    {
      if (max == -1)
	pb.pulse();
      else
	pb.set_fraction(static_cast<double>(cur) / max);
    }

    void set_text(const Glib::ustring& text)
    {
      label.set_text(text);
    }
  };
}
//------------------------------------------------------------------------------
HTMLView::TabManager::TabManager()
{
  set_show_tabs();
  signal_switch_page().connect(sigc::mem_fun(*this, &TabManager::switch_page));
  new_tab();
}
//------------------------------------------------------------------------------
Mozilla::WebControl* HTMLView::TabManager::new_tab(const Glib::ustring& url)
{
  Mozilla::WebControl* wc = manage(new Mozilla::WebControl());
  const int idx = append_page(*wc, *manage(new TabLabel("(no name)")));

  wc->signal_title().connect(sigc::bind<int>(sigc::mem_fun(*this, &TabManager::title), idx));
  wc->signal_location().connect(sigc::bind<int>(sigc::mem_fun(*this, &TabManager::location), idx));
  wc->signal_link_message().connect(sigc::bind<int>(sigc::mem_fun(*this, &TabManager::link_message), idx));
  wc->signal_new_window().connect(sigc::mem_fun(*this, &TabManager::new_window));
  wc->signal_net_state_all().connect(sigc::mem_fun(net_log, &NetLog::log_state));
  wc->signal_net_start().connect(sigc::bind<int>(sigc::mem_fun(*this, &TabManager::net_start), idx));
  wc->signal_net_stop().connect(sigc::bind<int>(sigc::mem_fun(*this, &TabManager::net_stop), idx));
  wc->signal_open_uri().connect(sigc::mem_fun(*this, &TabManager::open_uri));
  wc->signal_dom_mouse_click().connect(sigc::mem_fun(*this, &TabManager::dom_mouse_click));
  wc->signal_progress().connect(sigc::bind<int>(sigc::mem_fun(*this, &TabManager::progress), idx));
  wc->show();

  set_current_page(idx);

  if (url != "")
    load_in_current_tab(url);

  return wc;
}
//------------------------------------------------------------------------------
void HTMLView::TabManager::load_in_current_tab(const Glib::ustring& url)
{
  get_web_control().load_url(url);
}
//------------------------------------------------------------------------------
Mozilla::WebControl& HTMLView::TabManager::get_web_control()
{
  return dynamic_cast<Mozilla::WebControl&>(*get_nth_page(get_current_page()));
}
//------------------------------------------------------------------------------
void HTMLView::TabManager::title(int idx)
{
  const Glib::ustring title = get_web_control().get_title();
  dynamic_cast<TabLabel&>(*get_tab_label(*get_nth_page(idx))).set_text(title != "" ? title : "(no name)");

  if (idx == get_current_page()) 
    m_signal_title.emit(title);
}
//------------------------------------------------------------------------------
void HTMLView::TabManager::location(int idx)
{
  if (idx == get_current_page()) 
    m_signal_location.emit(get_web_control().get_location());
}
//------------------------------------------------------------------------------
void HTMLView::TabManager::link_message(int idx)
{
  if (idx == get_current_page()) 
    m_signal_status_message.emit(get_web_control().get_link_message());
}
//------------------------------------------------------------------------------
Mozilla::WebControl* HTMLView::TabManager::new_window(Mozilla::ChromeFlags)
{
  return new_tab();
}
//------------------------------------------------------------------------------
void HTMLView::TabManager::switch_page(GtkNotebookPage*, guint)
{
  m_signal_location.emit(get_web_control().get_location());
  m_signal_title.emit(get_web_control().get_title());
}
//------------------------------------------------------------------------------
bool HTMLView::TabManager::open_uri(const Glib::ustring& uri)
{
  try
    {
      if (Glib::RefPtr<Gnome::Vfs::Uri> u = Gnome::Vfs::Uri::create(uri))
	if (gvfs_load_schemes.find(u->get_scheme()) != gvfs_load_schemes.end())
	  return open_uri_with_gvfs(u);
	else if (external_schemes.find(u->get_scheme()) != external_schemes.end()
	    && (u->is_local() || (external_hosts.find(u->get_host_name()) != external_hosts.end())))
	  {
	    const Glib::ustring mime_type = u->get_file_info(Gnome::Vfs::FILE_INFO_GET_MIME_TYPE)->get_mime_type();
	    if (external_mimes.find(mime_type) != external_mimes.end())
	      if(Gnome::Vfs::MimeApplication app = Gnome::Vfs::Mime::get_default_application(mime_type))
		{
		  std::vector<Glib::ustring> uris(1);
		  uris[0] = uri;
		  app.launch(uris);
		  return true;
		}
	  }
    }
  catch (Gnome::Vfs::exception& e)
    {
      std::cerr << "Gnome::Vfs::exception: " << e.what() << std::endl;
    }
  return false;
}
//------------------------------------------------------------------------------
bool HTMLView::TabManager::open_uri_with_gvfs(Glib::RefPtr<Gnome::Vfs::Uri> uri)
{
  try
    {
      Gnome::Vfs::Handle h;
      h.open(uri, Gnome::Vfs::OPEN_READ);
      Glib::RefPtr<Gnome::Vfs::FileInfo> info = h.get_file_info(Gnome::Vfs::FILE_INFO_GET_MIME_TYPE);
      Mozilla::WebControl& wc = get_web_control();

      const guint block_sz = info->get_io_block_size();
      gpointer buffer = g_alloca(block_sz);

      {	
	Mozilla::WebControl::Stream stm(wc, uri->to_string(), info->get_mime_type());
	for (Gnome::Vfs::FileSize bytes_read = h.read(buffer, block_sz);
	     bytes_read != 0; bytes_read = h.read(buffer, block_sz))
	    wc.append_data(static_cast<char*>(buffer), bytes_read);
      }
      return true;
    }
  catch (Gnome::Vfs::exception& e)
    {
      std::cerr << "Gnome::Vfs::exception: " << e.what() << std::endl;
    }
  return false;
}
//------------------------------------------------------------------------------
gint HTMLView::TabManager::dom_mouse_click(nsCOMPtr<nsIDOMMouseEvent> dom_mouse_event)
{
  PRUint16 btn;
  dom_mouse_event->GetButton(&btn);
  if (btn == 2)
    {
      m_signal_popup.emit();
      return true;
    }
  return false;
}
//------------------------------------------------------------------------------
void HTMLView::TabManager::show_about()
{
  get_web_control().render_data("<html>"
				"  <head>"
				"    <title>About HTMLView</title>"
				"  </head>"
				"  <body>"
				"  <center>"
				"    <table border=\"0\">"
				"      <tr><td><img src=\"chrome://global/content/logo.gif\">"
				"      <td><font size=\"5\">HTMLView "VERSION"</font>"
				"      </tr>"
				"    </table><br>"
				"    HTMLView is a simple program to view HTML documents.<br>"
				"    It is written using gtkmozembedmm library."
				"  </center>"
				"  </body>"
				"</html>",
				"about:about", "text/html");
}
//------------------------------------------------------------------------------
void HTMLView::TabManager::progress(int cur, int max, int idx)
{
  dynamic_cast<TabLabel&>(*get_tab_label(*get_nth_page(idx))).update(cur, max);
}
//------------------------------------------------------------------------------
void HTMLView::TabManager::net_start(int idx) 
{ 
  m_signal_status_message.emit("Starting network transaction"); 
  dynamic_cast<TabLabel&>(*get_tab_label(*get_nth_page(idx))).show_progress();
}
//------------------------------------------------------------------------------
void HTMLView::TabManager::net_stop(int idx)
{ 
  m_signal_status_message.emit("Network transaction finished"); 
  dynamic_cast<TabLabel&>(*get_tab_label(*get_nth_page(idx))).hide_progress();
}
//------------------------------------------------------------------------------
