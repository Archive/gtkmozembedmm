/* 
 * NetLog.h - HTMLView network events log
 *
 * Copyright (C) 2004 Max Vasin
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */
//------------------------------------------------------------------------------
#include <sstream>
//------------------------------------------------------------------------------
#include "NetLog.h"
//------------------------------------------------------------------------------
namespace
{
  template <typename T>
  bool flag_set_p(T set, T flag)
  {
    return (set & flag) == flag;
  }

  Glib::ustring format(Mozilla::ProgressFlags state)
  {
    std::ostringstream stm;

    flag_set_p(state, Mozilla::WEB_CONTROL_FLAG_START) && stm << "start ";
    flag_set_p(state, Mozilla::WEB_CONTROL_FLAG_REDIRECTING) && stm << "redirecting ";
    flag_set_p(state, Mozilla::WEB_CONTROL_FLAG_TRANSFERRING) && stm << "transferring ";
    flag_set_p(state, Mozilla::WEB_CONTROL_FLAG_NEGOTIATING) && stm << "negotiating ";
    flag_set_p(state, Mozilla::WEB_CONTROL_FLAG_STOP) && stm << "stop ";
    flag_set_p(state, Mozilla::WEB_CONTROL_FLAG_IS_REQUEST) && stm << "is-request ";
    flag_set_p(state, Mozilla::WEB_CONTROL_FLAG_IS_DOCUMENT) && stm << "is-document ";
    flag_set_p(state, Mozilla::WEB_CONTROL_FLAG_IS_NETWORK) && stm << "is-network ";
    flag_set_p(state, Mozilla::WEB_CONTROL_FLAG_IS_WINDOW) && stm << "is-window ";

    return stm.str();
  }

  Glib::ustring format(Mozilla::StatusFlags status)
  {
    std::ostringstream stm;
    
    flag_set_p(status, Mozilla::WEB_CONTROL_STATUS_FAILED_DNS) && stm << "dns ";
    flag_set_p(status, Mozilla::WEB_CONTROL_STATUS_FAILED_CONNECT) && stm << "failed-connect ";
    flag_set_p(status, Mozilla::WEB_CONTROL_STATUS_FAILED_TIMEOUT) && stm << "failed-timeout ";
    flag_set_p(status, Mozilla::WEB_CONTROL_STATUS_FAILED_USERCANCELED) && stm << "user-canceled";

    return stm.str();
  }

}
//------------------------------------------------------------------------------
void HTMLView::NetLog::append_line(const Glib::ustring& text)
{
  log_buffer->insert(log_buffer->end(), text + "\n");
}
//------------------------------------------------------------------------------
HTMLView::NetLog::NetLog()
  : log_buffer(log_view.get_buffer())
{
  set_default_size(400, 400);
  set_title("NetLog");
  
  add(scroll_win);
  scroll_win.add(log_view);
  scroll_win.show_all();
}
//------------------------------------------------------------------------------
void HTMLView::NetLog::log_state(const Glib::ustring& aUri, Mozilla::ProgressFlags state, Mozilla::StatusFlags status)
{
  append_line("uri: " + aUri);
  append_line("state: " + format(state));
  append_line("status: " + format(status));
}
//------------------------------------------------------------------------------
