/* main.cc
 *
 * Copyright (C) 2004 Max Vasin
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gtkmm.h>
#include <gtkmozembedmm.h>

const Glib::ustring test_uri = "file:///usr/share/gtk-doc/html/gtk/index.html";

class Simple : public Gtk::Window
{
  Mozilla::WebControl wc;
public:
  Simple()
  {
    set_border_width(5);
    set_default_size(600, 400);
    set_title("Simple");
    add(wc);
    wc.show();
    wc.load_url(test_uri);
  }
};

int main (int argc, char** argv)
{
  Gtk::Main kit(argc, argv);
  Simple simple;
  Gtk::Main::run(simple);
  return 0;
}
