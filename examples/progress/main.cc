/* main.cc
 *
 * Copyright (C) 2004 Max Vasin
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <sstream>
#include <gtkmm.h>
#include <gtkmozembedmm.h>

const Glib::ustring test_uri = "http://www.gtkmm.org";

class Progress : public Gtk::Window
{
  Mozilla::WebControl wc;
  Gtk::VBox vbox;
  Gtk::Label progress_label;
public:
  Progress()
  {
    set_border_width(5);
    set_default_size(600, 400);
    set_title("Gtk::WebControl example");
    add(vbox);
    vbox.set_homogeneous(false);
    vbox.add(wc);
    vbox.pack_start(progress_label, false, false);
    vbox.show_all();
    wc.signal_progress().connect(sigc::mem_fun(*this, &Progress::show_progress));
    wc.signal_title().connect(sigc::mem_fun(*this, &Progress::change_title));
    wc.load_url(test_uri);
  }

private:
  void show_progress(gint curprogress, gint maxprogress)
  {
    std::ostringstream stm;
    stm << "got " << curprogress << " of ";
    if (maxprogress < 1)
      stm << "unknown";
    else
      stm << maxprogress;

    progress_label.set_text(stm.str());
  }

  void change_title()
  {
    set_title(wc.get_title());
  }
};

int main (int argc, char** argv)
{
  Gtk::Main kit(argc, argv);
  Progress progress;
  Gtk::Main::run(progress);
  return 0;
}
